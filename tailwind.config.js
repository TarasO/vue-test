/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
    theme: {
        extend: {
            colors: {
                "main-primary": "#35008a",
                "main-secondary": "#2d0071",
                "alt-primary": "#d08f00",
                "alt-secondary": "#d05700"
            },
        },
        fontFamily: {
            Roboto: ["Roboto, sans-serif"],
        },
        container: {
            padding: "2rem",
            center: true,
        },
        screens: {
            sm: "640px",
            md: "768px"
        }
    },
    plugins: [],
}
